# Ufirst

The repo contains only the client side of the exercise and it is build with React, React router to handle the navigation and MSW to simulate a server.

It works good on Chrome and it is not responsive yet.

# How to run it?

From a new terminal window clone the repo and install dependencies:

```
git clone https://gitlab.com/estefanodi/ufirst.git
cd ufirst
npm install
```

In order to run properly, the project needs a json file which could be created running a script, again from the terminal run 

```
npm run generate
```

Running the command above, it will create a json file located in the following location:

```
src/utils/node/data.json
```

after the json file is created, yuo can run the project through

```
npm start
```

The exercise has 4 screens: 

### One to visualize all the requests by minutes in the last 24 hours in a line chart

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1630758779/Screenshot_from_2021-09-04_14-09-27.png" width='100%' alt="main screen">

### One to visualize all the requests HTTP methods distributed in a pie chart

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1630758779/Screenshot_from_2021-09-04_14-09-31.png" width='100%' alt="main screen">

### One to visualize all the responses status codes distributed in a pie chart

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1630758779/Screenshot_from_2021-09-04_14-09-35.png" width='100%' alt="main screen">

### And the last to visualize all the responses with status code 200 and size less than 1000b in a bar chart

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1630758779/Screenshot_from_2021-09-04_14-09-38.png" width='100%' alt="main screen">


# Try a live version on surge

[go to surge page](https://godly-creature.surge.sh/ "surge")


# How to run the tests?

From a differnt terminal

```
npm run test
```