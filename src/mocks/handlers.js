import { rest } from "msw";

import {
  filterByMinutes,
  filterByMethods,
  filterByStatusCode,
  filterBySizeAndCode,
  getLayoutChartData
} from "../utils/functions/filters";

export const handlers = [
  rest.get("/get-by-minutes/:day/:hour", (req, res, ctx) => {
    const { day, hour } = req.params;
    return res(
      ctx.status(200),
      ctx.json({
        data: filterByMinutes(day, hour),
      })
    );
  }),
  rest.get("/get-by-methods", (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        data: filterByMethods(),
      })
    );
  }),
  rest.get("/get-by-response-codes", (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        data: filterByStatusCode(),
      })
    );
  }),
  rest.get("/get-by-size-and-200-response-code", (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        data: filterBySizeAndCode(),
      })
    );
  }),
  rest.get("/get-chart-filters-data", (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        data: [...getLayoutChartData()],
      })
    );
  }),
];
