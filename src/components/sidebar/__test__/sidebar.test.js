import Sidebar from "../index";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";

import { buttons } from "../../../layout/sidebar.json";

let getByTestId;

beforeEach(() => {
  const history = createMemoryHistory();
  const component = render(
    <Router history={history}>
      <Sidebar />
    </Router>
  );
  getByTestId = component.getByTestId;
});

test("it should test if the sidebar is rendered", () => {
  const sidebarEl = getByTestId("sidebar");
  expect(sidebarEl).toBeInTheDocument();
});

test("it should test the number of buttons rendered by the sidebar", () => {
  const buttonsList = screen.getAllByRole("button");
  expect(buttonsList.length).toBe(buttons.length);
  for (let i = 0; i < buttons.length; i++) {
    expect(buttonsList[i].textContent).toBe(buttons[i].title);
  }
});

//! the click of the buttons is tested in pages/router/__test__/app.test.js
