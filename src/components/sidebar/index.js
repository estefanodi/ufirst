import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

import Button from "../button";

import { buttons } from "../../layout/sidebar.json";

import "./assets/styles.css";

function Sidebar({ history }) {
  const handleClick = (path) => history.push(path);
  return (
    <div className="sidebar" data-testid="sidebar">
      {buttons.map((ele) => (
        <Button
          key={ele.id}
          {...ele}
          testid={`sidebar-button${ele.id}`}
          handleClick={handleClick}
        />
      ))}
    </div>
  );
}

export default withRouter(Sidebar);

Sidebar.defaultProps = {
  history: {},
};

Sidebar.propTypes = {
  history: PropTypes.object,
};
