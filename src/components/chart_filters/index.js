import { useState, useEffect } from "react";
import PropTypes from "prop-types";

import Button from "../button";

import "./assets/styles.css";

function ChartFilters({ chartFiltersData, fetchData }) {
  const [layoutData, setLayoutData] = useState([]);
  const [selectedDay, setSelectedDay] = useState("");
  const [selectedHour, setSelectedHour] = useState("");

  useEffect(() => {
    if (chartFiltersData) {
      setLayoutData(chartFiltersData);
      setSelectedDay(chartFiltersData[0]?.day);
      setSelectedHour(chartFiltersData[0]?.hour[0]);
    }
  }, [chartFiltersData]);
  
  useEffect(() => {
    if (selectedDay && selectedHour) {
      const path = `/get-by-minutes/${selectedDay}/${selectedHour}`;
      fetchData(path, "requestsPerMinute")
    }
  }, [selectedDay, selectedHour]);

  const handleHeaderClick = (day) => {
    setSelectedDay(day);
    setSelectedHour(chartFiltersData[0].hour[0]);
  };
  const handleBottomClick = (hour) => setSelectedHour(hour);

  return (
    <div className="chart-filters" data-testid="chart-filters">
      <div className="chart-filters-top" data-testid="chart-filters-top">
        {chartFiltersData.map(({ day }) => (
          <Button
            key={day}
            title={day + "/08/1995"}
            handleClick={() => handleHeaderClick(day)}
            args={[day]}
            classes={["filters-button", selectedDay === day && "active"]}
            testid={`filters-button${day}`}
          />
        ))}
      </div>
      <div className="chart-filters-bottom">
        {layoutData
          .find((ele) => ele.day === selectedDay)
          ?.hour.map((ele) => (
            <Button
              key={ele}
              title={ele + ":00"}
              handleClick={() => handleBottomClick(ele)}
              classes={["filters-button", selectedHour === ele && "active"]}
              testid={`filters-button${ele}`}
            />
          ))}
      </div>
    </div>
  );
}

export default ChartFilters;

ChartFilters.defaultProps = {
  chartFiltersData: [],
};

ChartFilters.propTypes = {
  chartFiltersData: PropTypes.array,
  fetchData: PropTypes.func.isRequired,
};
