import ChartFilters from "../index";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

const mockedClick = jest.fn();

let getByTestId;
beforeEach(() => {
  const component = render(
    <ChartFilters fetchData={mockedClick}/>
  );
  getByTestId = component.getByTestId;
});

test("it should test if the chart filters are rendered", () => {
    const chartEl = getByTestId("chart-filters");   
    expect(chartEl).toBeInTheDocument();
});
