import Button from "../index";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

const mockedClick = jest.fn();

let getByTestId;
beforeEach(() => {
  const component = render(
    <Button
      title={"Submit"}
      testid={"sidebar-button1"}
      type={"button"}
      handleClick={mockedClick}
      classes={["sidebar-button"]}
    />
  );
  getByTestId = component.getByTestId;
});

describe("test the button component", () => {
  test("it should test if the button is rendered", () => {
    const buttonEl = getByTestId("sidebar-button1");
    expect(buttonEl).toBeInTheDocument();
  });

  test("button title should be 'Submit' and type should be 'button'", () => {
    const buttonEl = getByTestId("sidebar-button1");
    expect(buttonEl.textContent).toBe("Submit");
    expect(buttonEl.type).toBe("button");
  });

  test("button className should be 'sidebar-button'", () => {
    const buttonEl = getByTestId("sidebar-button1");
    expect(buttonEl.className).toBe("sidebar-button");
  });

  test("it should call a function when the button is clicked", () => {
    const buttonEl = getByTestId(`sidebar-button1`);
    fireEvent.click(buttonEl);
    expect(mockedClick).toBeCalled();
  });
});
