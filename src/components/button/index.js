import PropTypes from "prop-types";

import "./assets/styles.css";

function Button({ title, handleClick, type, classes, args, testid }) {
  return (
    <button
      type={type}
      className={classes.join(" ")}
      data-testid={testid}
      onClick={() => handleClick(...args)}
    >
      {title}
    </button>
  );
}

export default Button;

Button.defaultProps = {
  title: "default title",
  type: "button",
  classes: [],
  args: [],
  testid: "sidebar-button1"
};

Button.propTypes = {
  title: PropTypes.string,
  handleClick: PropTypes.func.isRequired,
  type: PropTypes.string,
  classes: PropTypes.array,
  args: PropTypes.array,
  testid: PropTypes.string
};
