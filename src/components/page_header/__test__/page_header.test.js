import PageHeader from "../index";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

test("it should test if the component is rendered and its text content", () => {
  const { getByTestId } = render(
    <PageHeader title={"Distribution of HTTP methods"} />
  );
  const headerEl = getByTestId("page-header");
  expect(headerEl).toBeInTheDocument();
  expect(headerEl.textContent).toBe("Distribution of HTTP methods");
});
