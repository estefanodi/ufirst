import PropTypes from "prop-types";

import "./assets/styles.css";

function PageHeader({ title }) {
  return (
    <div className="page-header" data-testid="page-header">
      <span>{title}</span>
    </div>
  );
}

export default PageHeader;

PageHeader.defaultProps = {
  title: "Default title",
};

PageHeader.propTypes = {
  title: PropTypes.string,
};
