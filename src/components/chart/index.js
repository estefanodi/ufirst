import Chart from "react-google-charts";
import PropTypes from "prop-types";

import { chartOptions, chartMap } from "./helper";
import "./assets/styles.css";

function ChartComponent({ chartType, data, is3D, stateKey, testid }) {
  return (
    <div data-testid={testid} className="chart-container">
      <Chart
        width={"100%"}
        height={"100%"}
        chartType={chartType}
        loader={<div>Loading Chart</div>}
        data={[chartMap[stateKey], ...data]}
        options={{
          ...chartOptions[stateKey],
          is3D,
        }}
        rootProps={{ "data-testid": testid }}
      />
    </div>
  );
}

export default ChartComponent;

ChartComponent.defaultProps = {
  data: [],
  is3D: true,
  stateKey: "",
};

ChartComponent.propTypes = {
  chartType: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  is3D: PropTypes.bool,
  stateKey: PropTypes.string,
  testid: PropTypes.string,
};
