import ChartComponent from "../index";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

test("it should test if the chart is rendered", () => {
  const { getByTestId } = render(<ChartComponent testid={"methods"} chartType="PieChart"/>);
  const chartEl = getByTestId("methods");
  expect(chartEl).toBeInTheDocument();
});
