export const chartOptions = {
  requestsPerMinute: {
    hAxis: {
      title: "Minutes",
      minValue: 0
    },
    vAxis: {
      title: "Requests",
      minValue: 0,
    },
  },
  successResponses: {
    hAxis: {
      title: "Responses",
      minValue: 0,
    },
    vAxis: {
      title: "Bites",
      minValue: 0,
    },
  },
};

export const chartMap = {
  requestsPerMinute: ["Minutes", "Requests"],
  methods: ["Value", "Methods"],
  statusCodes: ["Value", "Status codes"],
  successResponses: ["Bites", "Responses"],
};
