import { render, screen, fireEvent } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

import "@testing-library/jest-dom";

import App from "../App";

beforeEach(() => {
  const history = createMemoryHistory();
  render(
    <Router history={history}>
      <App />
    </Router>
  );
});

test("it should test the initial page header text content", () => {
  expect(
    screen.getByText("Requests per minute over the entire time span")
  ).toBeInTheDocument();
});

test("it should test the page header text content after the 'HTTP methods' sidebar button is clicked", () => {
  const buttonsList = screen.getAllByRole("button");
  fireEvent.click(buttonsList[1]);
  expect(screen.getByText("Distribution of HTTP methods")).toBeInTheDocument();
});

test("it should test the page header text content after the 'Status codes' sidebar button is clicked", () => {
  const buttonsList = screen.getAllByRole("button");
  fireEvent.click(buttonsList[2]);
  expect(
    screen.getByText("Distribution of HTTP answer codes")
  ).toBeInTheDocument();
});

test("it should test the page header text content after the 'Successful responses' sidebar button is clicked", () => {
  const buttonsList = screen.getAllByRole("button");
  fireEvent.click(buttonsList[3]);
  expect(
    screen.getByText(
      "Distribution of responses with status code 200 and size less than 1000b"
    )
  ).toBeInTheDocument();
});
