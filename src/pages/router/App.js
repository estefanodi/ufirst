import { useState, useEffect } from "react";
import { Route, Switch, useLocation } from "react-router-dom";
import axios from "axios";

import SidebarPage from "../sidebar_page";
import NotFound from "../not_found";

import Sidebar from "../../components/sidebar";
import ChartFilters from "../../components/chart_filters";
import { sidebarRoutes } from "../../layout/routes.json";

function App() {
  const [data, setData] = useState({
    requestsPerMinute: [],
    methods: [],
    statusCodes: [],
    successResponses: [],
  });
  const [chartFiltersData, setChartFiltersData] = useState([]);
  const { pathname } = useLocation();

  useEffect(() => {
    fetchFiltersData();
  }, []);

  const fetchFiltersData = async () => {
    try {
      const response = await axios.get("/get-chart-filters-data");
      if (response.status === 200) {
        setChartFiltersData(response.data.data);
      }
    } catch (error) {}
  };

  const fetchData = async (path, stateKey) => {
    //* prevent to refetch in case the data are already in the state
    if (data[stateKey].length > 0 && stateKey !== "requestsPerMinute") return;
    try {
      const response = await axios.get(path);
      if (response.status === 200) {
        setData({ ...data, [stateKey]: response.data.data });
      }
    } catch (error) {}
  };
  return (
    <div className="main-container">
      <Sidebar />
      <Switch>
        {sidebarRoutes.map(
          ({ path, id, title, stateKey, requestPath, chartType }) => {
            return (
              <Route
                key={id}
                exact
                path={path}
                render={() => (
                  <SidebarPage
                    data={data[stateKey]}
                    fetchData={fetchData}
                    title={title}
                    stateKey={stateKey}
                    requestPath={requestPath}
                    chartType={chartType}
                  />
                )}
              />
            );
          }
        )}
        <Route component={NotFound} />
      </Switch>
      {pathname === "/" && (
        <ChartFilters
          data={data.requestsPerMinute}
          chartFiltersData={chartFiltersData}
          fetchData={fetchData}
        />
      )}
    </div>
  );
}

export default App;
