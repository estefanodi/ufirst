import { useEffect } from "react";
import PropTypes from "prop-types";

import PageHeader from "../../components/page_header";
import Chart from "../../components/chart";

import "./assets/styles.css";

function SidebarPage({
  data,
  fetchData,
  title,
  stateKey,
  requestPath,
  chartType,
}) {
  useEffect(() => {
    const getData = async () => {
      await fetchData(requestPath, stateKey);
    };
    getData();
  }, []);
  return (
    <div className="page-container">
      <PageHeader title={title} />
      <Chart
        data={data}
        chartType={chartType}
        testid={stateKey}
        is3D={true}
        stateKey={stateKey}
      />
    </div>
  );
}

export default SidebarPage;

SidebarPage.defaultProps = {
  data: [],
  title: "default title",
  stateKey: "",
  requestPath: "",
};

SidebarPage.propsTypes = {
  data: PropTypes.array,
  fetchData: PropTypes.func.isRequired,
  title: PropTypes.string,
  stateKey: PropTypes.string,
  requestPath: PropTypes.string,
};
