import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import "./assets/main.css";
import App from "./pages/router/App";
import reportWebVitals from "./reportWebVitals";

const { worker } = require("./mocks/browser");
worker.start();

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals(console.log);
