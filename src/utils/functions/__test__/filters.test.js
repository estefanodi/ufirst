import {
  getLayoutChartData,
  filterByMethods,
  filterByStatusCode,
  filterByMinutes,
  filterBySizeAndCode,
  removeFirstZero,
} from "../filters";
import { mockData, mockData2 } from "./mock_data";

const sorter = (arr) => {
  arr.sort((a, b) => {
    if (a[0] < b[0]) return -1;
    if (b[0] < a[0]) return 1;
    return 0;
  });
  return arr;
};
describe("it should test the helper functions: sorter and removeFirstZero", () => {
  test("it should test the sorter function", () => {
    const result = sorter([
      ["z", 10],
      ["c", 10],
      ["a", 10],
      ["b", 10],
    ]);
    expect(result[0][0]).toBe("a");
    expect(result[1][0]).toBe("b");
    expect(result[2][0]).toBe("c");
    expect(result[3][0]).toBe("z");
  });
  
  test("it should test the returned value of removeFirstZero function", () => {
    let result = removeFirstZero("15");
    expect(result).toBe("15");
    result = removeFirstZero("00");
    expect(result).toBe("0");
    result = removeFirstZero("03");
    expect(result).toBe("3");
  });
});

describe("it is testing the result of the filters layout elements", () => {
  test("it should test the returned array from the getLayoutChartData function", () => {
    const result = getLayoutChartData(mockData2);
    expect(result.length).toBe(2);
    expect(result[0].day).toBe("29");
    expect(result[1].day).toBe("30");
    expect(result[0].hour.length).toBe(1);
    expect(result[1].hour.length).toBe(1);
  });
});

describe("it is testing the filters functions", () => {
  test("it should test the returned array from the filterByMinutes function", () => {
    const result = filterByMinutes("29", "23", mockData2);
    expect(result.length).toBe(3);
    expect(result[0][0]).toBe("55");
    expect(result[0][1]).toBe(4);
    expect(result[1][0]).toBe("56");
    expect(result[1][1]).toBe(5);
    expect(result[2][0]).toBe("57");
    expect(result[2][1]).toBe(13);
  });

  test("it should test the returned array from the filterByMethods function", () => {
    const result = sorter(filterByMethods(mockData));
    expect(result.length).toBe(3);
    expect(result[0][0]).toBe("GET");
    expect(result[0][1]).toBe(18);
    expect(result[1][0]).toBe("HEAD");
    expect(result[1][1]).toBe(3);
    expect(result[2][0]).toBe("POST");
    expect(result[2][1]).toBe(1);
  });

  test("it should test the returned array from the filterByStatusCode function", () => {
    const result = sorter(filterByStatusCode(mockData));
    expect(result.length).toBe(2);
    expect(result[0][0]).toBe("200");
    expect(result[0][1]).toBe(19);
    expect(result[1][0]).toBe("302");
    expect(result[1][1]).toBe(3);
  });

  test("it should test the returned array from filterBySizeAndCode function", () => {
    const result = filterBySizeAndCode(mockData);
    expect(result.length).toBe(5);
    expect(result[0][0]).toBe("0-100b");
    expect(result[0][1]).toBe(1);
    expect(result[1][0]).toBe("200-300b");
    expect(result[1][1]).toBe(2);
    expect(result[2][0]).toBe("500-600b");
    expect(result[2][1]).toBe(1);
    expect(result[3][0]).toBe("600-700b");
    expect(result[3][1]).toBe(1);
    expect(result[4][0]).toBe("900-1000b");
    expect(result[4][1]).toBe(1);
  });
});
