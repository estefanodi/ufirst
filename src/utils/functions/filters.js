import data from "../node/data.json";

export const getLayoutChartData = (toFilter = data) => {
  const tempData = [];
  for (let i = 0; i < toFilter.length; i++) {
    if (toFilter[i].datetime.day) {
      const index = tempData.findIndex(
        (ele) => ele.day === toFilter[i].datetime.day
      );
      if (index === -1) {
        tempData.push({
          day: toFilter[i].datetime.day,
          hour: [toFilter[i].datetime.hour],
        });
      } else {
        const includes = tempData[index].hour.includes(
          toFilter[i].datetime.hour
        );
        if (!includes) {
          tempData[index].hour.push(toFilter[i].datetime.hour);
        }
      }
    }
  }
  return tempData;
};

export const removeFirstZero = (str) => {
  return str[0] === "0" && str[1] !== 0 ? str.substring(1) : str;
};

export const filterByMinutes = (day, hour, toFilter = data) => {
  const obj = {};
  for (let i = 0; i < toFilter.length; i++) {
    if (
      toFilter[i].datetime.day === day &&
      toFilter[i].datetime.hour === hour
    ) {
      obj[removeFirstZero(toFilter[i].datetime.minute)] =
        obj[toFilter[i].datetime.minute] + 1 || 1;
    }
  }
  return Object.entries(obj);
};

export const filterByMethods = (toFilter = data) => {
  const obj = {};
  for (let i = 0; i < toFilter.length; i++) {
    obj[toFilter[i].request.method] = obj[toFilter[i].request.method] + 1 || 1;
  }
  return Object.entries(obj);
};

export const filterByStatusCode = (toFilter = data) => {
  const obj = {};
  for (let i = 0; i < toFilter.length; i++) {
    obj[toFilter[i].response_code] = obj[toFilter[i].response_code] + 1 || 1;
  }
  return Object.entries(obj);
};

export const filterBySizeAndCode = (toFilter = data) => {
  const arr = [];
  for (let i = 0; i < toFilter.length; i++) {
    if (
      toFilter[i].response_code === "200" &&
      !isNaN(toFilter[i].document_size) &&
      parseInt(toFilter[i].document_size) < 1000
    ) {
      arr.push(parseInt(toFilter[i].document_size));
    }
  }
  const final = [];
  let startRange = 0;
  let endRange = 100;
  for (let i = 0; i < 10; i++) {
    const temp = arr.filter((int) => int > startRange && int < endRange);
    const str = startRange + "-" + endRange + "b";
    temp.length > 0 && final.push([str, temp.length]);
    startRange += 100;
    endRange += 100;
  }
  return final;
};
