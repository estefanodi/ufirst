const {
  getDatetime,
  getRequestData,
  getProtocol,
  removeUncommonCharacters,
} = require("../generate");

describe("it will test the functions used to generate the json file", () => {
  test("it should test what is returned from the getDatetime function", () => {
    let result = getDatetime("[29:13:45:36]");
    expect(result.day).toBe("29");
    expect(result.hour).toBe("13");
    expect(result.minute).toBe("45");
    expect(result.second).toBe("36");
    result = getDatetime("");
    expect(result).toBe("date not available");
  });

  test("it should test what is returned from the getRequestData function", () => {
    let result = getRequestData(
      '161.119.73.37 [30:18:57:19] "GET /logos/us-flag.gif HTTP/1.0" 304 0'
    );
    expect(result.method).toBe("GET");
    expect(result.url).toBe("/logos/us-flag.gif");
    expect(result.protocol).toBe("HTTP");
    expect(result.protocol_version).toBe("1.0");
    result = getRequestData(
      '161.119.73.37 [30:18:57:19] "/logos/us-flag.gif HTTP/1.0" 304 0'
    );
    expect(result.method).toBe("invalid request");
    expect(result.url).toBe("/logos/us-flag.gif");
    expect(result.protocol).toBe("HTTP");
    expect(result.protocol_version).toBe("1.0");
  });

  test("it should test what is returned from the getProtocol function", () => {
    let result = getProtocol("HTTP/1.0");
    expect(result.protocol).toBe("HTTP");
    expect(result.protocol_version).toBe("1.0");
    result = getProtocol("");
    expect(result.protocol).toBe("No protocol available");
    expect(result.protocol_version).toBe("No version available");
  });
});

describe("it should test the generate json helpers functions", () => {
  test("it should test what is returned from the removeUncommonCharacters function", () => {
    let result = removeUncommonCharacters(
      '161.119.73.37 [30:18:**57:19] "/logos/us~-flag.gif HTTP/1.0" 304 0**'
    );
    expect(result).toBe(
      '161.119.73.37 [30:18:57:19] "/logos/us-flag.gif HTTP/1.0" 304 0'
    );
  });
})
