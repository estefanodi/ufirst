const fs = require("fs");
const textData = __dirname + "/data.txt";

const getProtocol = (prot = "") => {
  if (prot.includes("HTTP")) {
    const splitted = prot.split("/");
    return {
      protocol: splitted[0],
      protocol_version: splitted[1],
    };
  } else {
    return {
      protocol: "No protocol available",
      protocol_version: "No version available",
    };
  }
};

const getRequestData = (str) => {
  const allowedMethods = ["POST", "GET", "HEAD"];
  const subString = str
    .substring(str.indexOf('"') + 1, str.lastIndexOf('"'))
    .split(" ");
  return {
    method: allowedMethods.includes(subString[0])
      ? subString[0]
      : "invalid request",
    url: allowedMethods.includes(subString[0]) ? subString[1] : subString[0],
    ...getProtocol(subString[subString.length - 1]),
  };
};

const getDatetime = (str) => {
  if (!str || str.length != 13) {
    return "date not available";
  } else {
    const splitted = str
      .substring(str.indexOf("[") + 1, str.lastIndexOf("]"))
      .split(":");
    return {
      day: splitted[0],
      hour: splitted[1],
      minute: splitted[2],
      second: splitted[3],
    };
  }
};

const removeUncommonCharacters = (str) => {
  return str.replace(/[\*+^${}()|~\\]/g, "");
};

const generateJsonFile = (data) => {
  const dataArr = data.split("\n");
  const dataToJson = [];
  for (let i = 0; i < dataArr.length; i++) {
    const splitted = removeUncommonCharacters(dataArr[i]).split(" ");
    const tempObj = {
      host: splitted[0],
      datetime: getDatetime(splitted[1]),
      request: getRequestData(dataArr[i]),
      response_code: splitted[splitted.length - 2],
      document_size: splitted[splitted.length - 1],
    };
    dataToJson.push(tempObj);
  }
  const jsonString = JSON.stringify(dataToJson, null, 4);
  return fs.writeFile(__dirname + "/data.json", jsonString, (err) => {
    return err ? false : true;
  });
};

const generateJson = (file = textData) => {
  return fs.readFile(file, "utf8", (err, data) => {
    if (err) throw err;
    return generateJsonFile(data);
  });
};
generateJson();

module.exports = {
  getDatetime,
  getRequestData,
  getProtocol,
  removeUncommonCharacters,
};
